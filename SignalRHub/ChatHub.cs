﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    public class ChatHub : Hub
    {

        private static Dictionary<string, string> users = new Dictionary<string, string>();
        
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task JoinRoom(string roomName)
        {
            await this.Groups.AddToGroupAsync(this.Context.ConnectionId, roomName);
        }

        public async Task LeaveRoom(string roomName)
        {
            await this.Groups.RemoveFromGroupAsync(this.Context.ConnectionId, roomName);
        }

        public async Task SendMessageToRoom(string user, string message, string roomName)
        {
            await Clients.Group(roomName).SendAsync("ReceiveMessage", user, message);
        }

        public async Task JoinChat(string user)
        {
            users[user] = this.Context.ConnectionId;

            await Groups.AddToGroupAsync(Context.ConnectionId, user);
        }

        public async Task SendMessageToUser(string userFrom, string message, string userTo)
        {
            if (users.ContainsKey(userTo))
            {
                await Clients.Client(users[userTo]).SendAsync("ReceiveMessage", userFrom, message);
            }
        }


    }
}