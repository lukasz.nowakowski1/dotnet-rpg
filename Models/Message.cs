using System;

namespace dotnet_rpg.Models
{
    public class Message
    {
        public int MessageId { get; set; }
        public string MessageText { get; set; }
        public User User { get; set; }
        public string UserName { get; set; }
        public string SendTo { get; set; }
        public string SendFrom { get; set; }
        public DateTime Date { get; set; }
    }
}