using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_rpg.Models
{
    public class MessageReceiver
    {
        public int ReceiverId { get; set; }

        public string Name { get; set; }

        public string emailaddress { get; set; }
    }
}