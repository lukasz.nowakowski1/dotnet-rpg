using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_rpg.Models
{
    public class MessageSender
    {
        public int SenderId { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }
    }
}