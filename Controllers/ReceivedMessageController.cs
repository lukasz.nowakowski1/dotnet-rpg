using dotnet_rpg.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_rpg.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]

    public class ReceivedMessageController : ControllerBase
    {
        private readonly IMessageService _messageService;

        public ReceivedMessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpPost("Test_Read")]
        public async Task<ActionResult<ServiceResponse<List<GetMessageDto>>>> ReadMessages()
        {    
            return Ok(await _messageService.GetAllMessages());
        }

        [HttpGet("SearchByDate")]
        public async Task<ActionResult<ServiceResponse<GetMessageDto>>> GetMessageByDate(System.DateTime date)
        {
            return Ok(await _messageService.GetMessageByDate(date));
        }
    }
}