using dotnet_rpg.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_rpg.Controllers
{

    [Authorize]
    [ApiController]
    [Route("[controller]")]

    public class MessageController : ControllerBase
    {
        private readonly MessageService _messageService;

        [HttpPost("Test_Send")]
        public async Task<ActionResult<ServiceResponse<List<GetMessageDto>>>> SendMessage(AddMessageDto newMessage)
        {
            return Ok(await _messageService.SendMessage(newMessage));
        }
    }
}