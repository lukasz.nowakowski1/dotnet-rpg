﻿using System;

public class GetMessageDto
{   
    public int Id { get; set; }
    public int SenderId { get; set; }
    public string MessageText { get; set; }
}
