﻿using System;

public class AddMessageDto
{
		public int Id { get; set; }
		public int ReceiverId { get; set; }
		public string MessageText { get; set; }
}
