﻿using dotnet_rpg.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public interface IMessageService
{
        Task<ServiceResponse<List<GetMessageDto>>> GetAllMessages();

        Task<ServiceResponse<GetMessageDto>> GetMessageByDate(System.DateTime date);

        Task<ServiceResponse<List<GetMessageDto>>> SendMessage(AddMessageDto newMessage);

        Task<ServiceResponse<List<GetMessageDto>>> DeleteMessage(int id);
}
