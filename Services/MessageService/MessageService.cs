﻿using AutoMapper;
using dotnet_rpg.Data;
using dotnet_rpg.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

public class MessageService : IMessageService
{
    private readonly DataContext _context;
    
    private readonly IMapper _mapper;
    
    private readonly IHttpContextAccessor _httpContextAccessor;

    public MessageService(IMapper mapper, DataContext context, IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
        _context = context;
        _mapper = mapper;
    }


    private int GetUserId()
    {
          var name = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
          if (!int.TryParse(name, out var result))
              return -1;
          return result;
    }

    public async Task<ServiceResponse<List<GetMessageDto>>> GetAllMessages()
    {
          var serviceResponse = new ServiceResponse<List<GetMessageDto>>();
          var dbMessages = await _context.Messages 
               .Where(m => m.User.Id == GetUserId()).ToListAsync();//to get all messages from database by User id
          serviceResponse.Data = dbMessages.Select(m => _mapper.Map<GetMessageDto>(m)).ToList();
          return serviceResponse;
     }

     public async Task<ServiceResponse<GetMessageDto>> GetMessageByDate(System.DateTime date)
     {
          var serviceResponse = new ServiceResponse<GetMessageDto>();
          var dbMessage = await _context.Messages
               .FirstOrDefaultAsync(m => m.Date == m.Date && m.User.Id == GetUserId());
          serviceResponse.Data = _mapper.Map<GetMessageDto>(dbMessage); //serviceResponse.Data is actual result
          return serviceResponse;
     }

    public async Task<ServiceResponse<List<GetMessageDto>>> SendMessage(AddMessageDto newMessage)
    {
        var serviceResponse = new ServiceResponse<List<GetMessageDto>>();
        Message message = _mapper.Map<Message>(newMessage);
        message.User = await _context.Users.FirstOrDefaultAsync(u => u.Id == GetUserId());

        _context.Messages.Add(message);
        await _context.SaveChangesAsync();
        serviceResponse.Data = await _context.Messages
                .Where(m => m.User.Id == GetUserId())
                .Select(m => _mapper.Map<GetMessageDto>(m)).ToListAsync();//adding srvcerespns data to character
        return serviceResponse; //returns ServiceResponse object  as response
    }

    public Task<ServiceResponse<List<GetMessageDto>>> DeleteMessage(int id)
    {
        throw new System.NotImplementedException();
    }
}
